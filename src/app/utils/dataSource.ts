
export interface DataSourceItem {
  key: string;
  value: string;
}

export interface AutoCompleteDataSource {
  search(input: string): DataSourceItem[]
}
