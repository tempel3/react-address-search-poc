export interface ValidationResult {
  message: string;
}

export interface Validateable {
  value: string;
}

export interface ValidationRule {
  validate(validateable: Validateable): ValidationResult | null;
}

export abstract class AbstractValidationRule implements ValidationRule {
  message: string;

  constructor(message: string) {
    this.message = message;
  }

  createResult() : ValidationResult {
    return { message: this.message };
  }

  abstract validate(validateable: Validateable): ValidationResult | null;
}

export class RequiredValidationRule extends AbstractValidationRule {

  validate(validateable: Validateable): ValidationResult | null {
    if (!validateable.value) return this.createResult();
    return null;
  }
}

export class RegExValidationRule extends AbstractValidationRule {
  expression: RegExp;

  constructor(expression: RegExp, message: string) {
    super(message);

    this.expression = expression;
  }

  validate(validateable: Validateable): ValidationResult | null {
    if (!this.expression.test(validateable.value)) return this.createResult();
    return null;
  }
}

export class Validator {

  rules: ValidationRule[] = [];

  addRule(rule: ValidationRule) {
    this.rules.push(rule);
  }

  validate(validateable: Validateable): ValidationResult[] {
    return this.rules.map(i=>i.validate(validateable)).filter(i=>i).map(i=>i as ValidationResult);
  }
}
