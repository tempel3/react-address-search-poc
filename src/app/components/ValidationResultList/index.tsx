import * as React from 'react';
import {
  Validator,
  RequiredValidationRule,
  ValidationResult
} from 'app/utils/validator';

export namespace ValidationResultList {
  export interface Props {
    results: ValidationResult[];
  }
}

export class ValidationResultList extends React.Component<ValidationResultList.Props> {
  validator: Validator = new Validator();

  constructor(props: ValidationResultList.Props, context?: any) {
    super(props, context);

    this.validator.addRule(new RequiredValidationRule('Pflicht!'));
  }

  render() {
    return (
      this.props.results.length > 0 && (
        <ul>
          {this.props.results.map((result) => (
            <li key={result.message}>{result.message}</li>
          ))}
        </ul>
      )
    );
  }
}
