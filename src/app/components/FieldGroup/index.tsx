import * as React from 'react';
import { ValidationResultList } from '../ValidationResultList';
import { FieldLabel } from '../FieldLabel';
import { ValidationResult } from 'app/utils/validator';
import * as classNames from 'classnames';
import * as style from './style.css';

export namespace FieldGroup {
  export interface Props {
    className?: string;
    fieldName: string;
    label?: React.Component;
    input?: JSX.Element;
    validate?: () => ValidationResult[];
  }
}

export class FieldGroup extends React.Component<FieldGroup.Props> {

  constructor(props: FieldGroup.Props, context?: any) {
    super(props, context);

  }

  render() {
    var validationResults = this.props.validate?this.props.validate():[];

    const classes = classNames(
      {
        [style.error]: validationResults.length > 0,
      },
      // style.normal
    );

    console.log("class names of field group", classes);

    return (<div className={classes}>
      <div>
    <FieldLabel fieldname={this.props.fieldName} value={this.props.fieldName}/>
    </div>
    <div>
    {this.props.input}
    </div>
    <ValidationResultList results={validationResults}/>
    </div>
    );
  }
}
