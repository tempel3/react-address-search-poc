import * as React from 'react';
import * as classNames from 'classnames';
import { Validator, Validateable, ValidationRule } from 'app/utils/validator';
import * as style from './style.css';
// import { ValidationResultList } from '../ValidationResultList';
// import { FieldLabel } from '../FieldLabel';
import { FieldGroup } from '../FieldGroup';

export namespace TextBox {
  export interface Props {
    value?: string;
    placeholder?: string;
    name: string;
    validaton?: ValidationRule[]
    // newTodo?: boolean;
    // editing?: boolean;
    onChange: (text: string) => void;
  }

  export interface State {
    value: string;
  }
}

export class TextBox extends React.Component<TextBox.Props, TextBox.State> implements Validateable {

  validator: Validator = new Validator();

  constructor(props: TextBox.Props, context?: any) {
    super(props, context);
    //this.state = { text: this.props.value || '' };
    this.handleBlur = this.handleBlur.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.props.validaton?.forEach(v => this.validator.addRule(v));
  }

  handleSubmit(event: React.KeyboardEvent<HTMLInputElement>) {
    const text = event.currentTarget.value.trim();
    if (event.which === 13) {
      this.props.onChange(text);
    }
  }

  handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ value: event.target.value });
  }

  handleBlur(event: React.FocusEvent<HTMLInputElement>) {
    const text = event.target.value.trim();
      this.props.onChange(text);
  }

  get value(): string {
    if(this.state?.value != null) return this.state.value;
    return this.props.value || '';
  };

  render() {

    var validationResults = this.validator.validate(this);

    const classes = classNames(
      {
        [style.error]: validationResults.length > 0,
      },
      // style.normal
    );

    return (
      <FieldGroup
      className={classes}
      fieldName={this.props.name}
      validate={() => this.validator.validate(this)}
      input={this.renderInner()}>
      </FieldGroup>
      // <div className={classes}>
      //   <div>
      // <FieldLabel fieldname={this.props.name} value={this.props.name}/>
      // </div>
      // <div>
      // <input
      //   name={this.props.name}
      //   type="text"
      //   placeholder={this.props.placeholder}
      //   value={this.value}
      //   onBlur={this.handleBlur}
      //   onChange={this.handleChange}
      //   onKeyDown={this.handleSubmit}
      // />
      // </div>
      // <ValidationResultList results={validationResults}/>
      // </div>
    );
  }


  renderInner() {
    return (<input
        name={this.props.name}
        type="text"
        placeholder={this.props.placeholder}
        value={this.value}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
        onKeyDown={this.handleSubmit}
      />);
  }
}
