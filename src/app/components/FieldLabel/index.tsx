import * as React from 'react';

export namespace FieldLabel {
  export interface Props {
    fieldname: string;
    value: string;
  }
}

export class FieldLabel extends React.Component<FieldLabel.Props> {

  constructor(props: FieldLabel.Props, context?: any) {
    super(props, context);

  }

  render() {
    return <label htmlFor={this.props.fieldname}>{this.props.value}</label>;
  }
}
