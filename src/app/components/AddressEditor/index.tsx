import * as React from 'react';
import { Autocomplete } from '../Autocomplete';
import { AutoCompleteDataSource, DataSourceItem } from 'app/utils/dataSource';
import { TextBox } from '../TextBox';
import { RequiredValidationRule, RegExValidationRule } from 'app/utils/validator';

export interface Address {
  city: string;
  street: string;
  houseNumber: string;
}

export namespace AddressEditor {
  export interface Props {
    address?: Address;
    onAddress: (address: Address) => any;
  }

  export interface State {
    city: string;
    street: string;
    houseNumber: string;
  }
}

export class AddressEditor extends React.Component<AddressEditor.Props, AddressEditor.State> {

  /**
   *
   */
  constructor(props: AddressEditor.Props, context?: any) {
    super(props, context);

    // TODO wird probleme machen, darf nicht direkt gesetzt werden!
    this.state = {
      city: this.props.address?.city??'',
      street: this.props.address?.street??'',
      houseNumber: this.props.address?.houseNumber??''
    };

    this.setCity = this.setCity.bind(this);
    this.setStreet = this.setStreet.bind(this);
    this.setHouseNumber = this.setHouseNumber.bind(this);
  }

  setCity(city: string) {
    this.setState({ city: city, street: '', houseNumber: ''});

    var address = this.getAddress();
    address.city = city;

    this.checkAndFireAddress(address);
  }

  setStreet(street: string) {
    this.setState({ street: street, houseNumber: ''});

    var address = this.getAddress();
    address.street = street;
    this.checkAndFireAddress(address);
  }

  setHouseNumber(houseNumber: string) {
    this.setState({ houseNumber: houseNumber});

    var address = this.getAddress();
    address.houseNumber = houseNumber;
    this.checkAndFireAddress(address);
  }

  getAddress() : Address {
    return {city: this.state.city, street: this.state.street, houseNumber: this.state.houseNumber}
  }

  checkAndFireAddress(address: Address) {

    if(address.city && address.houseNumber && address.street) {
      this.props.onAddress(address);
    }
  }

  render() {
    let cities = ['Hamburg', 'Dortmund', 'Bochum'];
    let allStreets = {'Hamburg': ["Moorlilientwiete", "Millerntorplatz"], 'Dortmund':["Flamingoweg"], 'Bochum': ["Universitätsstraße"]};
    let streets : string[] = [];

    // bäh
    switch(this.state.city) {
      case 'Hamburg': streets = allStreets.Hamburg; break;
      case 'Dortmund': streets = allStreets.Dortmund; break;
      case 'Bochum': streets = allStreets.Bochum; break;
    }

    let citiesDataSource: AutoCompleteDataSource = {
      search: (input) => cities.filter(i=> input && i.toLocaleLowerCase().startsWith(input.toLocaleLowerCase())).map<DataSourceItem>(i=> ({ key: i, value: i}))
    };

    let streetsDataSource: AutoCompleteDataSource = {
      search: (input) => streets.filter(i=> input && i.toLocaleLowerCase().startsWith(input.toLocaleLowerCase())).map<DataSourceItem>(i=> ({ key: i, value: i}))
    };

    // fileterOptions() {
    //   return this.props.options.filter((i) => this.getFilterString() && i.toLowerCase().startsWith(this.getFilterString().toLowerCase()));
    // }

    return (
      <div>
        <div>
          <h2>Editor</h2>
          <div>
            Ort:
            <Autocomplete value={this.state.city} dataSource={citiesDataSource}  selected={this.setCity} />
          </div>
          {
            streets.length > 0 && (
              <div>
                <div>Straße</div>
                <Autocomplete value={this.state.street} dataSource={streetsDataSource} selected={this.setStreet} />
                <TextBox
                  name="HouseNumber"
                  value={this.state.houseNumber}
                  onChange={this.setHouseNumber}
                  validaton={[new RequiredValidationRule("Pflicht!"), new RegExValidationRule(/^\d+.*$/,"Die Hausnummer muss mit einer Zahl anfangen.")]}
                />
              </div>
            )
        }
        </div>

        <div>
          <h2>View</h2>
          <div>City: {this.state.city}</div>
          <div>Straße: {this.state.street}</div>
          <div>Hausnummer: {this.state.houseNumber}</div>
        </div>
      </div>
    );
  }
}
