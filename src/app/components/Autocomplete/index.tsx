import * as React from 'react';
import { AutoCompleteDataSource, DataSourceItem } from 'app/utils/dataSource';

export namespace Autocomplete {
  export interface Props {
    value: string;
    //options: string[];
    dataSource: AutoCompleteDataSource,
    selected: (item: string) => any;
  }

  export interface State {
    text: string;
    selected: boolean;
  }
}

export class Autocomplete extends React.Component<Autocomplete.Props, Autocomplete.State> {
  constructor(props: Autocomplete.Props, context?: any) {
    super(props, context);
    this.state = { text: '', selected: false };

    this.handleBlur = this.handleBlur.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    // console.log("new state", event.target.value)
    this.setState({ text: event.target.value, selected: false });
    this.props.selected('');
  }

  handleBlur(event: React.FocusEvent<HTMLInputElement>) {
    // console.log("blur");
    // if (!this.state.selected) this.setState({ text: event.target.value, selected: false });
  }

  fileterOptions(): DataSourceItem[] {
    //console.log("fileterOptions", this.getFilterString(), this.props.dataSource.search(this.getFilterString()));
    return this.props.dataSource.search(this.getFilterString());

  }

  handleSubmit(event: React.KeyboardEvent<HTMLInputElement>) {
    if (event.which === 13) {
      let filteredOptions = this.fileterOptions();

      if (filteredOptions.length > 0) {
        this.onSelected(filteredOptions[0].key);
      }
    }
  }

  onSelected(item: string) {
    this.setState({ text: item, selected: true });
    this.props.selected(item);
  }

  getFilterString()
  {
    return this.state.text??this.props.value;
  }

  render() {
    let filteredOptions = this.fileterOptions();

    return (
      <div>
        <input
          // className={classes}
          type="text"
          autoFocus
          // placeholder={this.props.placeholder}
          value={this.getFilterString()}
          onBlur={this.handleBlur}
          onChange={this.handleChange}
          onKeyDown={this.handleSubmit}
        />
        {!this.state.selected && filteredOptions.length > 0 && (
          <div>
            <ul>
              {filteredOptions.map((option) => (
                <li key={option.key} onClick={(i) => this.onSelected(option.value)}>
                  {option.value}
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
    );
  }
}
